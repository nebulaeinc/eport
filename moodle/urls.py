from django.conf.urls import url
from . import views
from moodle import moodlepull
from django.contrib.auth import views as auth_views



urlpatterns = [
    url(r'^moodlemanager/$', views.moodlemanager, name="moodlemanager"),
    url(r'^dispatch/$', moodlepull.dispatch, name="dispatch"),
    url(r'^populategradetable/$', moodlepull.populategradetable, name="populategradetable"),
]
