# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.core import serializers
from django import forms
from acounts.models import *
from students.models import *
import json
from bson import json_util
import urllib2
import students
#import pdb; pdb.set_trace()



@login_required
def dispatch(request):
    populategradetable(request)
    html = getmoddata(request)
    return HttpResponse("html")



#get the data for a single module
# need user id & course id (moodle)
@login_required
def populategradetable(request):
    uid = request.user.id
    student = Studentdb.objects.get(mysqlid = uid.__str__())
    token = student.token
    userid = student.userid
    moduleid = request.GET['objectid']
    mod = Moduledb.objects.get(id=moduleid)
    courseid = mod.module.modid
    response = urllib2.urlopen('http://127.0.1.1/moodle/webservice/rest/server.php?wstoken={0}&wsfunction=gradereport_user_get_grades_table&moodlewsrestformat=json&courseid={1}&userid={2}'. format(token, courseid ,userid))
    data = json.load(response)
    tabledata = data['tables'][0]['tabledata']
    genHeader(request, tabledata[0], mod)
    itt = 1
    for row in tabledata[1:]:
        genRow(request, row, mod, itt)
        itt += 1
    return HttpResponse("success")
    #with that done use the modules EmbeddedDocument


@login_required
def genHeader(request, row, mod):
    header = Coursepiecedb(header= "True", title = row['itemname']['content'], position = 0 )
    mod.update(add_to_set__piece = header)
    mod.save()
    return True


@login_required
def genRow(request, row, mod, itt):
    row = Coursepiecedb(header= "False", title = row['itemname']['content'], position = itt, grade = row['grade']['content'], feedback = row['feedback']['content'] )
    mod.update(add_to_set__piece = row)
    mod.save()
    return True


@login_required
def getmoddata(request):
    modid = moduleid = request.GET['objectid']
    mod = Moduledb.objects.get(id = modid)
    pieces = mod.piece
    html = "<div>"
    for row in mod:
        html+= row.content
    html += "</div>"
    return HttpResponse(html)

#get the data for all mdoules of a user
@login_required
def popallgradetables(request, moodleid):
    return HttpResponse("sd")

def generategradetable(request):
    #check if the module data is populated
    #if not run populate grade table
    #Then  ----
    #get user id
    #find module
    #generate table and send html
    return HttpResponse("sd")


@login_required
def assign_mods_user(request, token, userid):
    userid = 4
    response = urllib2.urlopen('http://127.0.1.1/moodle/webservice/rest/server.php?wstoken={0}&wsfunction=gradereport_overview_get_course_grades&moodlewsrestformat=json&userid={1}'. format(token, userid))
    data = json.load(response)
    #get user object to add to mooutline focus
    user = Studentdb.objects.get(userid = userid.__str__())
    timeline = Usertimelinedb.objects.get(user = user)

    # for each module in data add the user to the module focus
    # then update the marks for that user
    print(data)
    for mod in data['grades']:
        modedit = mod['courseid']
        #add the user to list [] reference field
        out = Modoutlinedb.objects.get(modid = modedit.__str__())
        out.update(add_to_set__focus = user)
        # populate the moduledb table and at the module ref to it
        modulegrade = mod['grade']
        public = "pvt"
        position= "1"
        #cr
        import pdb; pdb.set_trace()
        modrecord = Moduledb( position = position.__str__() , modulegrade = modulegrade.__str__(), public = public.__str__())
        modrecord.module = out

        timeline.update(add_to_set__modules=modrecord)
        timeline.save()

        out.update(add_to_set__focus = user)
        out.save()

        #{u'courseid': 4, u'grade': u'-', u'rawgrade': None}
    return HttpResponse("kk")
