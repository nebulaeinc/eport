# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.core import serializers
from django import forms
from acounts.models import *
from students.models import *
import json
from bson import json_util
import urllib2
import students
#import pdb; pdb.set_trace()

"""
All the API calls needed for the Moodle VLE
Each call pulls data and populates the mongodb store
have checks to check all edge cases
update mysql token - mongodb connection
"""

""" Samples ....
saving to db
p = Test(username=username, function='createTimeline', script='<script type = "text/javascript" async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>', html='<div id="twitterCreateTimeline"> <a class="twitter-timeline" href="https://twitter.com/'+  username  +'">Tweets by'+ username +'</a></div>')
p.save()

              ********************************

getting from db
p = Posttweet(username=username, function='PostTweet', script='<script type = "text/javascript" async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>', html='<div id="twitterPostTweet"> <a href="https://twitter.com/share" class="twitter-share-button" data-show-count="false">Tweet</a></div>').save()
x = Posttweet.objects.filter(username=username)[0]
"""

@login_required
def moodlemanager(request):
    import pdb; pdb.set_trace()
    success = 1
    username = request.GET['username']
    password = request.GET['password']
    uniqid = request.GET['uniqid']

    import pdb; pdb.set_trace()

    token = moodlegettoken(request, username , password)

    import pdb; pdb.set_trace()
    userid = moodlegetusersetup(request, token, username , uniqid)

    import pdb; pdb.set_trace()
    """
    This logic is to load modules for moodle. Only admin can generate this
    make a sub page for admin to automate certain functionality
    """

    import pdb; pdb.set_trace()

    current_user = request.user
    user = AuthUser.objects.get(id=current_user.id)
    modlist = ""

    import pdb; pdb.set_trace()
    if user.tokenised == 0:
        if current_user.id == 1:
            modlist = moodlegetlistmods(request, token, userid)
            user.tokenised = 0 # change to 1
            user.save()
        else:
            modlist = assign_mods_user(request, token, userid)
    else:
        pass


    mods = assign_mods_user(request, token, userid)
    user.tokenised = 1
    user.save()
    #return redirect('/students/', request=request)
    #return HttpResponseRedirect(reverse('students', args=(request)))
    #return redirect('students', request = request)
    return HttpResponse("success")

#student db is saved
@login_required
def moodlegettoken(request, username, password):
    try:
        import pdb; pdb.set_trace()
        response = urllib2.urlopen('http://127.0.1.1/moodle/login/token.php?username={0}&password={1}&service=gp'. format(username, password))
        import pdb; pdb.set_trace()
        data = json.load(response)
        import pdb; pdb.set_trace()
        token = data['token']
        print("the token is " + token)
        import pdb; pdb.set_trace()
        return token
    except Exception as e:
        r = traceback.format_exc()
        return r

#, token, username, uniqid   moodlegetusersetup
@login_required
def moodlegetusersetup(request, token, username, uniqid ):
    try:
        #response = urllib2.urlopen('http://127.0.1.1/moodle/webservice/rest/server.php?wstoken=0a167eb2ab71bf9c143b22157ffa5a20&wsfunction=core_user_get_users_by_field&moodlewsrestformat=json&field=username&values[0]=teacher')
        response = urllib2.urlopen('http://127.0.1.1/moodle/webservice/rest/server.php?wstoken={0}&wsfunction=core_user_get_users_by_field&moodlewsrestformat=json&field=username&values[0]={1}'.  format(token, username))
        data = json.load(response)
        """
        Data population
        """
        userid = data[0]['id']
        firstname = data[0]['firstname']
        lastname = data[0]['lastname']
        coverletter = ""
        university = Universitydb.objects.get(uniqid = uniqid)
        banner = ""
        age = ""
        year = ""
        workexperience = ""
        email = data[0]['email']

        """
        Studentdb setup
        """
        current_user = request.user
        mysqlid = AuthUser.objects.get(id=current_user.id)
        c = Studentdb.objects(userid = userid.__str__(), username = username.__str__() , mysqlid = mysqlid.id.__str__() , token= token.__str__()).modify(upsert=True, new=True, set__userid = userid.__str__(), set__username = username.__str__(), set__mysqlid = mysqlid.id.__str__(), set__token = token.__str__())
        c.save()
        user = c

        """
        Usertimelinedb setup
        """
        u = Usertimelinedb.objects(userid = userid.__str__(), firstname = firstname.__str__(), lastname = lastname.__str__(), coverletter = coverletter.__str__(), banner = banner.__str__(), age = age.__str__(), year = year.__str__(), email = email.__str__() ).modify(upsert=True, new=True, set__userid = userid.__str__(), set__firstname = firstname.__str__(), set__lastname = lastname.__str__(), set__coverletter = coverletter.__str__(), set__banner = banner.__str__(), set__age = age.__str__(), set__year = year.__str__(),  set__email = email.__str__())
        #, firstname = firstname.__str__(), lastname = lastname.__str__(), coverletter = coverletter.__str__(),
        #banner = banner.__str__(),  age = age.__str__(), year = year.__str__() , email = email.__str__())
        u.user = user
        u.university = university
        u.save()
        return HttpResponse(userid)
    except Exception as e:
        r = traceback.format_exc()
        return HttpResponse("ssa")

"""
Global token required for this
"""
@login_required
def moodlegetlistmods(request, token, userid):

    response = urllib2.urlopen('http://127.0.1.1/moodle/webservice/rest/server.php?wstoken={0}&wsfunction=core_enrol_get_users_courses&moodlewsrestformat=json&userid={1}'. format(token, 4))
    data = json.load(response)


    for mod in data:
        modid = mod['id']
        shortname = mod['shortname']
        fullname = mod['fullname']
        modidtwo = mod['idnumber']
        summary = mod['summary']
        #mf = Modoutlinedb(modid = modid.__str__(), shortname = shortname.__str__(), fullname = fullname.__str__(), modidtwo = modidtwo.__str__(), summary = summary )
        #mf.save()
        mf = Modoutlinedb.objects(modid = modid.__str__(), shortname = shortname.__str__(), fullname = fullname.__str__(), modidtwo = modidtwo.__str__(), summary = summary ).modify(upsert=True, new=True, set__modid = modid.__str__(), set__shortname = shortname.__str__(), set__fullname = fullname.__str__(), set__modidtwo = modidtwo.__str__(), set__summary = summary)
        mf.save()
    return "courses loaded"

@login_required
def assign_mods_user(request, token, userid):
    response = urllib2.urlopen('http://127.0.1.1/moodle/webservice/rest/server.php?wstoken={0}&wsfunction=gradereport_overview_get_course_grades&moodlewsrestformat=json&userid={1}'. format(token, userid))
    data = json.load(response)
    #get user object to add to mooutline focus
    user = Studentdb.objects.get(userid = userid.__str__())
    timeline = Usertimelinedb.objects.get(user = user)

    # for each module in data add the user to the module focus
    # then update the marks for that user
    print(data)
    for mod in data['grades']:
        modedit = mod['courseid']
        #add the user to list [] reference field
        out = Modoutlinedb.objects.get(modid = modedit.__str__())
        out.update(add_to_set__focus = user)
        # populate the moduledb table and at the module ref to it
        modulegrade = mod['grade']
        public = "pvt"
        position= "1"
        #cr
        import pdb; pdb.set_trace()
        modrecord = Moduledb( position = position.__str__() , modulegrade = modulegrade.__str__(), public = public.__str__())
        modrecord.module = out
        modrecord.save()

        import pdb; pdb.set_trace()
        timeline.update(add_to_set__modules=modrecord)
        timeline.save()

        out.update(add_to_set__focus = user)
        out.save()

        #{u'courseid': 4, u'grade': u'-', u'rawgrade': None}
    return HttpResponse("kk")

@login_required
def moodlegetoverviewmarks(request, token, userid):
    response = urllib2.urlopen('http://127.0.1.1/moodle/webservice/rest/server.php?wstoken={0}&wsfunction=gradereport_overview_get_course_grades&moodlewsrestformat=json&userid={1}'. format(token, userid))
    data = json.load(response)
    marks = data['grades'][1]['grade']
    return marks

"""
overview + marks + assessed work
gets all the marks for the course which is not ideal
"""
def moodlegetgradetable():
    return HttpResponse("token")
