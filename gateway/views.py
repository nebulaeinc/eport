# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.core import serializers
# Create your views here.
from students import views as st


# -----   Determin if company or student ---- redirect to those apps
# --- import student
# --- import companies


@login_required
def eport(request):
    data = st.students(request)
    return HttpResponse(data)
    #return render(request, 'gateway/userportal.html', {'section': 'eport'})


def gateway(request):
    return render(request, 'gateway/userportal.html')


def studentreg(request):
    return HttpResponse("it worked")
