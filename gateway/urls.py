from django.conf.urls import url
from . import views
from django.contrib.auth import views as auth_views



urlpatterns = [
    url(r'^$', views.gateway, name="gateway"),
    url(r'^eport/$', views.eport, name="eport"),
    url(r'^studentreg/$', views.studentreg, name="studentreg"),
]
