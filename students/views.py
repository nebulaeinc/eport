# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.core import serializers
from django import forms
from acounts.models import *
from students.models import *
import json
from bson import json_util
import urllib2
# Create your views here.
#import pdb; pdb.set_trace()


@login_required
def tokenreq(request):
    return HttpResponse("hello")

@login_required
def students(request):
    """
    if else statement to see if the student has loged in for the token
    """
    current_user = request.user
    user = AuthUser.objects.get(id=current_user.id)
    if (user.tokenised == 0):
        form = NameForm()
        return render(request, 'students/name.html', {'form': form})
    else:
        return render(request, 'students/eport.html', {'section': 'students'})


class NameForm(forms.Form):
    unis = Universitydb.objects()
    uni_list = []
    itt = 0
    for x in unis:
        uni_list.append( ("x.uniqid", "x.uniname") )
        itt += 1

    #your_uni = forms.ModelChoiceField(queryset=Universitydb.objects.all())

    #your_uni = forms.ModelChoiceField(queryset=Universitydb.objects.all().values_list('uniname'))
    #your_uni = forms.ChoiceField(choices=Universitydb.objects.values_list('uniname'), label="", initial='', widget=forms.Select(), required=True)
    #universities = forms.ChoiceField(choices=uni_list, required=True )
    your_name = forms.CharField(label='Username', max_length=100)
    your_pass = forms.CharField(widget=forms.PasswordInput, label='Password', max_length=100)

@login_required
def uni(request):
    term = request.GET.get('query', '')
    posterms = Universitydb.objects.filter(uniname__contains = term)

    unilist = []
    for uni in posterms:
        ut = { "value": uni.uniname, "data": uni.uniqid}
        unilist.append(ut)

    #posterms = Universitydb.objects.filter(tags__in = term)
    #city_array = ["greece","germany","gambone","grenoa"]
    #json_cities = json.dumps(city_array)
    city = {"query": "Unit","suggestions": unilist  }
    return JsonResponse(city)

"""
class CountryAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not self.request.user.is_authenticated():
            return Country.objects.none()

        qs = Country.objects.all()

        if self.q:
            qs = qs.filter(name__istartswith=self.q)

        return qs
"""

@login_required
def test(request):
    #term = request.GET.get('query', '')
    term = 'Univ'
    posterms = Universitydb.objects.filter(uniname__contains = term)
    #unilist = []
    #for pos in posterms:
        #unilist.append(pos.uniname)

    return HttpResponse(posterms[0].uniname)


@login_required
def router(request):
    return HttpResponse("determine which uni/VLE the student is at using")


def dispatcher(request):
    return HttpResponse("Dispatch student data to the correct app to fire off events")

@login_required
def getcoverletter(request):
    userid = request.user.id
    student  = Studentdb.objects.get(mysqlid = userid.__str__())
    ut = Usertimelinedb.objects.get(user = student)
    coverletter = ut.coverletter
    return HttpResponse(coverletter)

@login_required
def setcoverletter(request):
    coverletter = request.GET['x']
    userid = request.user.id
    student  = Studentdb.objects.get(mysqlid = userid.__str__())
    ut = Usertimelinedb.objects.get(user = student)
    ut.coverletter = coverletter
    ut.save()
    return HttpResponse("sucess")

@login_required
def savedegree(request):
    degree = request.POST['degree']
    userid = request.user.id
    student  = Studentdb.objects.get(mysqlid = userid.__str__())
    ut = Usertimelinedb.objects.get(user = student)
    ut.coursestr = degree
    ut.save()
    return HttpResponse("sucess")

@login_required
def loadmodule(request):
    return render(request, 'students/module.html', {'section': 'module'})

@login_required
def summerimgupload(request):
    return HttpResponse("http://res.cloudinary.com/dhasw4cz8/image/upload/v1490558303/tuning-2157354_640_lzgjwa.jpg")
    """
    Sample db entries
    posterms = Universitydb.objects.filter(uniname__contains = term)
    posterms[0].uniname
    """
