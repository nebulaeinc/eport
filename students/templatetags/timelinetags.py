from __future__ import unicode_literals
from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.core import serializers
from django import forms
from acounts.models import *
from students.models import *
import json
from bson import json_util
import urllib2


from django import template
#import pdb; pdb.set_trace()


register = template.Library()

@register.simple_tag(takes_context=True)
def name_tag(context, userid):
    student = Studentdb.objects.get(mysqlid = userid.__str__())
    ut = Usertimelinedb.objects.get(user = student)
    name = ut.firstname + " " + ut.lastname
    return name


@register.simple_tag(takes_context = True)
def uni_tag(context, userid):
    student  = Studentdb.objects.get(mysqlid = userid.__str__())
    ut = Usertimelinedb.objects.get(user = student)
    uniname = ut.university.uniname
    return uniname.__str__()


@register.simple_tag(takes_context = True)
def degree_tag(context, userid):
    return  "d"

@register.simple_tag(takes_context = True)
def img_tag(context, userid):
    student  = Studentdb.objects.get(mysqlid = userid.__str__())
    ut = Usertimelinedb.objects.get(user = student)
    unilogo = ut.university.icon
    return unilogo.__str__()


@register.simple_tag(takes_context = True)
def cover_tag(context, userid):
    student  = Studentdb.objects.get(mysqlid = userid.__str__())
    ut = Usertimelinedb.objects.get(user = student)
    unilogo = "<div id='summernote'></div>"
    return unilogo.__str__()


@register.simple_tag(takes_context = True)
def get_mods(context, userid):
    student  = Studentdb.objects.get(mysqlid = userid.__str__())
    ut = Usertimelinedb.objects.get(user = student)
    mods = ut.modules
    return mods


@register.simple_tag(takes_context = True)
def get_degree(context, userid):
    student  = Studentdb.objects.get(mysqlid = userid.__str__())
    ut = Usertimelinedb.objects.get(user = student)
    degree = ut.coursestr
    return degree

@register.simple_tag(takes_context = True)
def get_mongo_id(context, document):
    return document.id
