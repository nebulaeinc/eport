from __future__ import unicode_literals
from django.db import models
from mongoengine import *
from mongoengine import connect
connect('eportdb')

# Create your models here.
class Studentdb(Document):
    userid = StringField()
    username = StringField()
    mysqlid = StringField()
    token = StringField()


class Vle(Document):
    name = StringField()
    version = StringField()
    url = StringField()
    service = StringField()
    other = StringField()

class Universitydb(Document):
    uniname = StringField()
    uniqid = StringField()
    unioutline = StringField()
    address = StringField()
    focus = ListField(ReferenceField(Studentdb))                    #focus
    vle = ReferenceField(Vle)
    icon = StringField()

class Coursedb(Document): #ref
    name = StringField()
    outline = StringField()
    focus = ListField(ReferenceField(Studentdb))                    #focus

class Modoutlinedb(Document): #ref
    modid  = StringField()
    shortname = StringField()
    fullname = StringField()
    modidtwo = StringField()
    summary = StringField()
    focus = ListField(ReferenceField(Studentdb))
    icon = StringField()                  #focus

class Coursepiecedb(EmbeddedDocument):
    header = StringField() #game design
    title = StringField()
    content = StringField()
    grade = StringField()
    feedback = StringField()
    graderange = StringField()
    position = IntField()
    piecetype = StringField()
    mediaurl = StringField()

class Skilldb(Document): #ref
    title = StringField()
    img = StringField()
    outline = StringField()

class Postdb(EmbeddedDocument):
    jobtitle = StringField()
    jobref = IntField()
    joboutline = StringField()
    datesent = DateTimeField()
    timeout = DateTimeField()

class Companydb(Document):
    company = StringField()
    banner = StringField()
    about = StringField()
    posts = ListField(EmbeddedDocumentListField(Postdb))
    focus =  ListField(ReferenceField(Studentdb))                   #focus

class Referencesdb(EmbeddedDocument):
    person = StringField()
    reference = StringField()
    email = StringField()
    company = ListField(ReferenceField(Companydb))

class Moduledb(Document):
    position = IntField()
    modulegrade = StringField()
    piece = ListField(EmbeddedDocumentListField(Coursepiecedb))
    references = ListField(EmbeddedDocumentField(Referencesdb))
    module = ReferenceField(Modoutlinedb)
    public = StringField()

class Workexpdb(EmbeddedDocument):
    position = IntField()
    company = ListField(EmbeddedDocumentField(Coursepiecedb))
    companytxt = StringField()
    companyabt = StringField()
    role = StringField()
    period = DateTimeField()
    references = ListField(EmbeddedDocumentField(Referencesdb))

class Usertimelinedb(Document):
    userid = StringField()
    user = ReferenceField(Studentdb)
    firstname = StringField()
    lastname = StringField()
    coverletter = StringField()
    university = ReferenceField(Universitydb)
    banner = StringField()
    age =  StringField()
    year =  StringField()
    email = StringField()
    coursestr = StringField()
    references = ListField(EmbeddedDocumentListField(Referencesdb))
    course = ListField(ReferenceField(Coursedb))
    modules = ListField(ReferenceField(Moduledb))
    skills = ListField(ReferenceField(Skilldb))
    workexp = ListField(EmbeddedDocumentListField(Workexpdb))
    newsfeed = ListField(EmbeddedDocumentListField(Postdb))



"""
Tags & Focuses

The main idea is that tags allow for the algorith to search for ideal candidates based on Tags
it should be able to create and delete tags and assign them to a user.

The idea of focuses is to know where to send of applications initially.
focuses are more to do with simple matchmaking rather than complex machine learning
they are always a good starting point

Module company university and course should each have their own ListField of studednts that have subscribed


Initial search == skills + focuses

solutin is to have a field that can store type company , uni , course and
"""

"""
class Focusesdb(Document):
    modfocuses = ListField(EmbeddedDocumentListField(Modoutlinedb))
    unifocus = ListField(EmbeddedDocumentField(Universitydb))
    coursefocus = ListField(EmbeddedDocumentListField(Moduledb))


    #req = ListField(EmbeddedDocumentField(Skilldb))"""
