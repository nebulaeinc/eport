from django.conf.urls import url
from . import views
from django.contrib.auth import views as auth_views



urlpatterns = [
    url(r'^$', views.students, name="students"),
    url(r'^tokenreq/$', views.tokenreq, name="tokenreq"),
    url(r'^test/$', views.test, name="test"),
    url(r'^uni/$', views.uni, name="uni"),
    url(r'^getcoverletter/$', views.getcoverletter, name="getcoverletter"),
    url(r'^setcoverletter/$', views.setcoverletter, name="setcoverletter"),
    url(r'^summerimgupload/$', views.summerimgupload, name="summerimgupload"),
    url(r'^savedegree/$', views.savedegree, name="savedegree"),

]
